import install from './install';

import 'mapbox-gl/dist/mapbox-gl.css';

// Export each components separately

// Export the install function as default
export default install;
export * from './components';
export * from './classes';

export { default as Mapbox } from 'mapbox-gl';
export { default as MapboxGeocoder } from '@mapbox/mapbox-gl-geocoder';
